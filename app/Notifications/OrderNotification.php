<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class OrderNotification extends Notification
{
    use Queueable;
    private $notificationOrder;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($notificationOrder)
    {
        $this->notificationOrder = $notificationOrder;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
        ->greeting($this->notificationOrder['greetings'])
        ->line($this->notificationOrder['body'])
        ->action($this->notificationOrder['actionText'], $this->notificationOrder['actionURL'])
        ->line($this->notificationOrder['thanks']);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'order_id' => $this->notificationOrder['order_id']
        ];
    }
}
