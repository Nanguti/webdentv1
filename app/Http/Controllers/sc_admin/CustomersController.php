<?php

namespace App\Http\Controllers\Sc_admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
//models
use App\Models\Customers;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\UploadCustomers;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
     {
         $this->middleware('auth');
     }
     
    public function index()
    { 
        
        $customers = new Customers();
        return view('auth-admin.customers.list',[
            'customers'=>$customers->getCustomers(),
         ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('isAdmin')) { 
            abort(403);
        }
        return view('auth-admin.customers.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('isAdmin')) { 
            abort(403);
        }
        
        $this->validate($request,[
        'full_name'=>'required',
        'mobile_no'=>'required',
        'location'=>'required'
        ]);

        $customer = new Customers();
        $customer->full_name = $request->input('full_name');
        $customer->mobile_no = $request->input('mobile_no');
        $customer->location = $request->input('location');
        $customer->added_by = auth()->user()->id;
        $customer->save();
        return redirect('/auth-admin.customers')->with('success','customer added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function import(){

      return view('auth-admin.customers.upload');
    }


    public function uploadcsv(Request $request){

       Excel::import(new UploadCustomers, $request->file('file')->store('temp'));
        return back();
    }
}
